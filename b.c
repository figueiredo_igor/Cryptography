#include <stdio.h>
#include <stdlib.h>

const int t = 2;

typedef struct{
  int chave, eVogal;
  float freq;
} TL;

typedef struct ArvB{
  int nchaves, folha;
  TL **letras;
  struct ArvB **filho;
}TAB;


TAB *Cria(int t){
  TAB* novo = (TAB*)malloc(sizeof(TAB));
  novo->nchaves = 0;
  novo->letras =(TL**)malloc(sizeof(TL*)*((t*2)-1));
  novo->folha=1;
  novo->filho = (TAB**)malloc(sizeof(TAB*)*t*2);
  int i;
  for(i=0; i<(t*2); i++) novo->filho[i] = NULL;
  return novo;
}


TAB *Libera(TAB *a){
  if(a){
    if(!a->folha){
      int i;
      for(i = 0; i <= a->nchaves; i++) Libera(a->filho[i]);
    }
    free(a->letras);
    free(a->filho);
    free(a);
    return NULL;
  }
}


void Imprime(TAB *a, int andar){
  if(a){
    int i,j;
    for(i=0; i<=a->nchaves-1; i++){
      Imprime(a->filho[i],andar+1);
      for(j=0; j<=andar; j++) printf("   ");
      int aux = a->letras[i]->chave;
      printf("%c\n", aux);
    }
    Imprime(a->filho[i],andar+1);
  }
}


TAB *Busca(TAB* x, int ch){
  TAB *resp = NULL;
  if(!x) return resp;
  int i = 0;
  while(i < x->nchaves && ch > x->letras[i]->chave) i++;
  if(i < x->nchaves && ch == x->letras[i]->chave) return x;
  if(x->folha) return resp;
  return Busca(x->filho[i], ch);
}

TL *Busca_Letra(TAB* x, int ch){
  TAB *resp = Busca(x, ch);
  if(!resp) return NULL;
  int i = 0;
  while(ch < resp->letras[i]->chave) i++;
  return resp->letras[i];
}


TAB *Inicializa(){
  return NULL;
}


TAB *Divisao(TAB *x, int i, TAB* y, int t){
  TAB *z=Cria(t);
  z->nchaves= t - 1;
  z->folha = y->folha;
  int j;
  for(j=0;j<t-1;j++) z->letras[j] = y->letras[j+t];
  if(!y->folha){
    for(j=0;j<t;j++){
      z->filho[j] = y->filho[j+t];
      y->filho[j+t] = NULL;
    }
  }
  y->nchaves = t-1;
  for(j=x->nchaves; j>=i; j--) x->filho[j+1]=x->filho[j];
  x->filho[i] = z;
  for(j=x->nchaves; j>=i; j--) x->letras[j] = x->letras[j-1];
  x->letras[i-1] = y->letras[t-1];
  x->nchaves++;
  return x;
}


TAB *Insere_Nao_Completo(TAB *x, TL* letra, int t){
  int i = x->nchaves-1;
  if(x->folha){
    while((i>=0) && (letra->chave<x->letras[i]->chave)){
      x->letras[i+1] = x->letras[i];
      i--;
    }
    x->letras[i+1] = letra;
    x->nchaves++;
    return x;
  }
  while((i>=0) && (letra->chave<x->letras[i]->chave)) i--;
  i++;
  if(x->filho[i]->nchaves == ((2*t)-1)){
    x = Divisao(x, (i+1), x->filho[i], t);
    if(letra->chave>x->letras[i]->chave) i++;
  }
  x->filho[i] = Insere_Nao_Completo(x->filho[i], letra, t);
  return x;
}

TL *Cria_Letra(int k, int freq, int eVogal){
  TL* novo = (TL*)malloc(sizeof(TL));
  novo->chave = k;
  novo->eVogal = eVogal;
  novo->freq = freq;
  return novo;
}


TAB *Insere(TAB *T, TL *letra, int t){
  if(Busca(T,letra->chave)) return T;
  if(!T){
    T=Cria(t);
    T->letras[0] = letra;
    
    T->nchaves=1;
    return T;
  }
  if(T->nchaves == (2*t)-1){
    TAB *S = Cria(t);
    S->nchaves=0;
    S->folha = 0;
    S->filho[0] = T;
    S = Divisao(S,1,T,t);
    S = Insere_Nao_Completo(S,letra,t);
    return S;
  }
  T = Insere_Nao_Completo(T,letra,t);
  return T;
}


TAB* remover(TAB* arv, int ch, int t){
  if(!arv) return arv;
  int i;
  printf("Removendo ..\n");
  for(i = 0; i<arv->nchaves && arv->letras[i]->chave < ch; i++);
  if(i < arv->nchaves && ch == arv->letras[i]->chave){ //CASOS 1, 2A, 2B e 2C
    if(arv->folha){ //CASO 1
      printf("\nCASO 1\n");
      int j;
      for(j=i; j<arv->nchaves-1;j++) arv->letras[j] = arv->letras[j+1];
      arv->nchaves--;
      return arv;      
    }
    if(!arv->folha && arv->filho[i]->nchaves >= t){ //CASO 2A
      printf("\nCASO 2A\n");
      TAB *y = arv->filho[i];  //Encontrar o predecessor k' de k na árvore com raiz em y
      while(!y->folha) y = y->filho[y->nchaves];
      TL *temp = y->letras[y->nchaves-1];
      arv->filho[i] = remover(arv->filho[i], temp->chave, t); 
      //Eliminar recursivamente K e substitua K por K' em x
      arv->letras[i] = temp;
      return arv;
    }
    if(!arv->folha && arv->filho[i+1]->nchaves >= t){ //CASO 2B
      printf("\nCASO 2B\n");
      TAB *y = arv->filho[i+1];  //Encontrar o sucessor k' de k na árvore com raiz em y
      while(!y->folha) y = y->filho[0];
      TL *temp = y->letras[0];
      y = remover(arv->filho[i+1], temp->chave, t); //Eliminar recursivamente K e substitua K por K' em x
      arv->letras[i] = temp;
      return arv;
    }
    if(!arv->folha && arv->filho[i+1]->nchaves == t-1 && arv->filho[i]->nchaves == t-1){ //CASO 2C
      printf("\nCASO 2C\n");
      TAB *y = arv->filho[i];
      TAB *z = arv->filho[i+1];
      y->letras[y->nchaves] = Busca_Letra(arv, ch);          //colocar ch ao final de filho[i]
      int j;
      for(j=0; j<t-1; j++)                //juntar chave[i+1] com letras[i]->chave
        y->letras[t+j] = z->letras[j];
      for(j=0; j<=t; j++)                 //juntar filho[i+1] com filho[i]
        y->filho[t+j] = z->filho[j];
      y->nchaves = 2*t-1;
      for(j=i; j < arv->nchaves-1; j++)   //remover ch de arv
        arv->letras[j] = arv->letras[j+1];
      for(j=i+1; j <= arv->nchaves; j++)  //remover ponteiro para filho[i+1]
        arv->filho[j] = arv->filho[j+1];
      arv->filho[j] = NULL; //Campello
      arv->nchaves--;
      arv->filho[i] = remover(arv->filho[i], ch, t);
      return arv;   
    }   
  }

  TAB *y = arv->filho[i], *z = NULL;
  if (y->nchaves == t-1){ //CASOS 3A e 3B
    if((i < arv->nchaves) && (arv->filho[i+1]->nchaves >=t)){ //CASO 3A
      printf("\nCASO 3A: i menor que nchaves\n");
      z = arv->filho[i+1];
      y->letras[t-1] = arv->letras[i];   //dar a y a chave i da arv
      y->nchaves++;
      arv->letras[i] = z->letras[0];     //dar a arv uma chave de z
      int j;
      for(j=0; j < z->nchaves-1; j++)  //ajustar chaves de z
        z->letras[j] = z->letras[j+1];
      //z->letras[j]->chave = 0; Rosseti
      y->filho[y->nchaves] = z->filho[0]; //enviar ponteiro menor de z para o novo elemento em y
      for(j=0; j < z->nchaves; j++)       //ajustar filhos de z
        z->filho[j] = z->filho[j+1];
      z->nchaves--;
      arv->filho[i] = remover(arv->filho[i], ch, t);
      return arv;
    }
    if((i > 0) && (!z) && (arv->filho[i-1]->nchaves >=t)){ //CASO 3A
      printf("\nCASO 3A: i igual a nchaves\n");
      z = arv->filho[i-1];
      int j;
      for(j = y->nchaves; j>0; j--)               //encaixar lugar da nova chave
        y->letras[j] = y->letras[j-1];
      for(j = y->nchaves+1; j>0; j--)             //encaixar lugar dos filhos da nova chave
        y->filho[j] = y->filho[j-1];
      y->letras[0] = arv->letras[i-1];              //dar a y a chave i da arv
      y->nchaves++;
      arv->letras[i-1] = z->letras[z->nchaves-1];   //dar a arv uma chave de z
      y->filho[0] = z->filho[z->nchaves];         //enviar ponteiro de z para o novo elemento em y
      z->nchaves--;
      arv->filho[i] = remover(y, ch, t);
      return arv;
    }
    if(!z){ //CASO 3B
      if(i < arv->nchaves && arv->filho[i+1]->nchaves == t-1){
        printf("\nCASO 3B: i menor que nchaves\n");
        z = arv->filho[i+1];
        y->letras[t-1] = arv->letras[i];     //pegar chave [i] e coloca ao final de filho[i]
        y->nchaves++;
        int j;
        for(j=0; j < t-1; j++){
          y->letras[t+j] = z->letras[j];     //passar filho[i+1] para filho[i]
          y->nchaves++;
        }
        if(!y->folha){
          for(j=0; j<t; j++){
            y->filho[t+j] = z->filho[j];
          }
        }
        for(j=i; j < arv->nchaves-1; j++){ //limpar referências de i
          arv->letras[j] = arv->letras[j+1];
          arv->filho[j+1] = arv->filho[j+2];
        }
        arv->nchaves--;
        arv = remover(arv, ch, t);
        return arv;
      }
      if((i > 0) && (arv->filho[i-1]->nchaves == t-1)){ 
        printf("\nCASO 3B: i igual a nchaves\n");
        z = arv->filho[i-1];
        if(i == arv->nchaves)
          z->letras[t-1] = arv->letras[i-1]; //pegar letras[i]->chave e poe ao final de filho[i-1]
        else
          z->letras[t-1] = arv->letras[i];   //pegar chave [i] e poe ao final de filho[i-1]
        z->nchaves++;
        int j;
        for(j=0; j < t-1; j++){
          z->letras[t+j] = y->letras[j];     //passar filho[i+1] para filho[i]
          z->nchaves++;
        }
        if(!z->folha){
          for(j=0; j<t; j++){
            z->filho[t+j] = y->filho[j];
          }
        }
        arv->nchaves--;
        arv->filho[i-1] = z;
        arv = remover(arv, ch, t);
        return arv;
      }
    }
  }  
  arv->filho[i] = remover(arv->filho[i], ch, t);
  return arv;
}


TAB* retira(TAB* arv, int k, int t){
  if(!arv || !Busca(arv, k)) return arv;
  return remover(arv, k, t);
}


int main(int argc, char *argv[]){
  TAB * arvore = Inicializa();
  int num = 0, from, to;
  FILE* arq = fopen("frequencia.txt", "r+");
  while(num != -1){
    printf("Digite uma numero para adicionar. 0 para imprimir. -9 para remover e -1 para sair\n");
    scanf("%i", &num);
    if(num == -9){
      scanf("%d", &from);
      arvore = retira(arvore, from, t);
      Imprime(arvore,0);
    }
    else if(num == -1){
      printf("\n");
      Imprime(arvore,0);
      Libera(arvore);
      return 0;
    }
    else if(!num){
      printf("\n");
      Imprime(arvore,0);
    }
    else{
      char *l;
      char *aux = (char*)malloc(sizeof(char)*8);;
      char *aux2 = (char*)malloc(sizeof(char)*7);
      float freq;
      int eVogal = 0;
      fread(l, sizeof(char),1,arq);
      fread(aux, sizeof(char*), 1, arq);
      int i = 0;
      while(aux[i] != '%') {
        aux2[i] = aux[i];
        i++;
      }
      if(*l == 'a' || *l == 'e' || *l == 'i' || *l == 'o' || *l == 'u') eVogal = 1;
      freq = atof(aux2);
      TL* letra = Cria_Letra(*l, freq, eVogal);
      arvore = Insere(arvore, letra, t);
    }
    printf("\n\n");
  }
}
